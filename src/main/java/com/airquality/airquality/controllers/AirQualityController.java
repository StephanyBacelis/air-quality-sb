package com.airquality.airquality.controllers;

import com.airquality.airquality.applications.AirQualityApplication;
import com.airquality.airquality.dao.HeatMapData;

import com.mashape.unirest.http.exceptions.UnirestException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/air-quality")
public class AirQualityController {

    @Autowired
    private AirQualityApplication airQualityApplication;

    @GetMapping("/country")
    public HeatMapData getByCountryAndMeasure(
            @RequestParam("countryCode") String countryCode,
            @RequestParam("measureParameter") String measureParameter
    ) throws UnirestException {
        return airQualityApplication.getByCountryCodeAndMeasureParameter(countryCode, measureParameter);
    }

    @GetMapping("/coordinates")
    public HeatMapData getByDegreeCoordinatesRadiusAndMeasure(
            @RequestParam("latitude") String latitude,
            @RequestParam("longitude") String longitude,
            @RequestParam("radius") String radius,
            @RequestParam("measureParameter") String measureParameter
    ) throws UnirestException {
        return airQualityApplication.getByCoordinatesRadiusAndMeasureParameter(latitude, longitude, radius, measureParameter);
    }
}
