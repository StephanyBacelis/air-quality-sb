package com.airquality.airquality.applications;

import com.airquality.airquality.dao.Data;
import com.airquality.airquality.dao.HeatMapData;
import com.airquality.airquality.dao.MeasurementResponse;
import com.airquality.airquality.dao.MeasurementResult;

import com.google.gson.Gson;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@Service
public class AirQualityApplication {

    public HeatMapData transformData(MeasurementResponse measurementResponse) {

        if ( measurementResponse == null ||
                measurementResponse.getResults() == null ||
                measurementResponse.getResults().isEmpty()) {
            return null;
        }

        ArrayList<String> scaleColors = new ArrayList<>();

        scaleColors.add("rgba(0, 0, 255, .251)");
        scaleColors.add("rgba(0, 255, 255, .3765)");
        scaleColors.add("rgba(50,205,50, .2675)");
        scaleColors.add("rgba(255, 255, 0, .7059)");
        scaleColors.add("rgba(255, 0, 0, .7843)");

        HeatMapData heatMapData = new HeatMapData();
        heatMapData.setCountryCode(measurementResponse.getResults().get(0).getCountry());
        heatMapData.setParameter(measurementResponse.getResults().get(0).getParameter());
        heatMapData.setScaleColors(scaleColors);

        ArrayList<Data> dataList = new ArrayList<>();

        for (MeasurementResult result: measurementResponse.getResults()) {
            Data data = new Data();

            data.setCoordinates(result.getCoordinates());
            data.setDate(result.getDate());
            data.setValue(result.getValue());

            dataList.add(data);
        }

        heatMapData.setData(dataList);

        return heatMapData;
    }

    public HeatMapData getByCountryCodeAndMeasureParameter(String countryCode, String measureParameter) throws UnirestException {

        MeasurementResponse measurementResponse = getData(countryCode, measureParameter);

        return transformData(measurementResponse);
    }

    public HeatMapData getByCoordinatesRadiusAndMeasureParameter(String latitude, String longitude, String radius, String measureParameter) throws UnirestException {

        MeasurementResponse measurementResponse = getData(latitude, longitude, radius, measureParameter);

        return transformData(measurementResponse);
    }

    public MeasurementResponse getData(String countryCode, String measureParameter) throws UnirestException {
        String url = "https://docs.openaq.org/v2/measurements?date_from=2000-01-01T00%3A00%3A00%2B00%3A00&date_to=2022-02-02T20%3A35%3A00%2B00%3A00&limit=100&page=1&offset=0&sort=desc&parameter="+measureParameter+"&radius=1000&country_id="+countryCode+"&order_by=datetime";

        return setData(url);
    }

    public MeasurementResponse getData(String latitude, String longitude, String radius, String measureParameter) throws UnirestException {
        String url = "https://docs.openaq.org/v2/measurements?date_from=2000-01-01T00%3A00%3A00%2B00%3A00&date_to=2022-02-03T18%3A19%3A00%2B00%3A00&limit=100&page=1&offset=0&sort=desc&parameter="+measureParameter+"&coordinates="+latitude+"%2C"+longitude+"&radius="+radius+"&order_by=datetime";

        return setData(url);
    }

    public MeasurementResponse setData(String url) throws UnirestException {
        Unirest.setTimeouts(30000, 30000);
        String response = Unirest.get(url).asString().getBody();
        Gson gson = new Gson();

        return gson.fromJson(response, MeasurementResponse.class);
    }
}
