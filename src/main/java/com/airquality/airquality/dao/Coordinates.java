package com.airquality.airquality.dao;

import lombok.Getter;
import lombok.Setter;

public class Coordinates {

    @Getter @Setter
    private Double latitude;

    @Getter @Setter
    private Double longitude;
}
