package com.airquality.airquality.dao;

import lombok.Getter;
import lombok.Setter;

public class MeasurementMeta {

    @Getter @Setter
    private String license;

    @Getter @Setter
    private String website;

    @Getter @Setter
    private Long found;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private Integer limit;

    @Getter @Setter
    private Integer page;
}
