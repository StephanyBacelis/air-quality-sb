package com.airquality.airquality.dao;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class MeasurementResponse {

    @Getter @Setter
    private MeasurementMeta meta;

    @Getter @Setter
    private List<MeasurementResult> results;
}
