package com.airquality.airquality.dao;

import lombok.Getter;
import lombok.Setter;

public class Data {

    @Getter @Setter
    private Coordinates coordinates;

    @Getter @Setter
    private Date date;

    @Getter @Setter
    private Double value;
}
