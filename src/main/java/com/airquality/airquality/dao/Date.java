package com.airquality.airquality.dao;

import lombok.Getter;
import lombok.Setter;

public class Date {

    @Getter @Setter
    private String utc;

    @Getter @Setter
    private String local;
}
