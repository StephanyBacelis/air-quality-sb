package com.airquality.airquality.dao;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class HeatMapData {

    @Getter @Setter
    private String parameter;

    @Getter @Setter
    private String countryCode;

    @Getter @Setter
    private List<Data> data;

    @Getter @Setter
    private List<String> scaleColors;
}
