package com.airquality.airquality.dao;

import lombok.Getter;
import lombok.Setter;

public class MeasurementResult {

    @Getter @Setter
    private Date date;

    @Getter @Setter
    private String country;

    @Getter @Setter
    private Boolean isAnalysis;

    @Getter @Setter
    private String city;

    @Getter @Setter
    private Coordinates coordinates;

    @Getter @Setter
    private String unit;

    @Getter @Setter
    private Integer locationId;

    @Getter @Setter
    private String parameter;

    @Getter @Setter
    private String sensorType;

    @Getter @Setter
    private String location;

    @Getter @Setter
    private Boolean isMobile;

    @Getter @Setter
    private Double value;

    @Getter @Setter
    private String entity;
}
