<h1> Air Quality API </h1>

Please find the instructions bellow to run this project.

###Prerequisites

1. Java installed & JAVA_HOME variable properly configured.
1. Terminal/CMD

###Windows

1. Go to folder `scripts/`
1. Double click file `run_windows.bat`
1. Wait until the command line displays a message similar to _`Started AirqualityApplication`_.
1. Go to [API Documentation](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config).

###Linux/Mac

1. In the terminal, go to folder `scripts/`
1. Run the command `sh run_linux.sh`
1. Wait until the command line displays a message similar to _`Started AirqualityApplication`_.
1. Go to [API Documentation](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config).

###The Worst Case

Assuming any of the scripts failed. 
To run this project manually follow the next steps:
1. Go to the project's folder.
1. Run the build with the following command.
<br>
    a. Windows: `gradlew clean build`
    <br>
    b. Linux/Mac: `./gradlew clean build`
1. Go to the folder `build/libs/`
1. Verify the jar exists. (_airquality-0.0.1-SNAPSHOT.jar_)
1. Run the command: `java -jar airquality-0.0.1-SNAPSHOT.jar`


Once the API is running, and you have accessed to the documentation, you can try out the endpoints.

####Documentation is now showing?

An alternative to consume the API endpoints could be via cURL.
You only need to support cURL calls in a terminal/command line. 

- First endpoint expects from the user a country code in two letters (E.g. US), 
    and a measure type (E.g. pm25).  
```
curl -X GET \
  'http://localhost:8080/air-quality/country?countryCode=MX&measureParameter=pm25'
```
- Second endpoint expects from the user the coordinates (latitude and longitude individually, e.g. latitude=18.5199&longitude=-88.30388), 
                      radius from specified coordinates (E.g. 1500), and a measure type (E.g. pm25).  
```
curl -X GET \
  'http://localhost:8080/air-quality/coordinates?latitude=18.5199&longitude=-88.30388&radius=1500&measureParameter=pm25'
```
---
<h5>Author: Stephany Bacelis</h5>
<h5>Email: stephanybacelis@gmail.com</h5>
